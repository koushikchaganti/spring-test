package com.springtest.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Method;

@RestController
@RequestMapping(value = "/app")
public class DemoController {

    @RequestMapping(method=RequestMethod.GET, value = "/test")
    public String testString(){
        return "Working";
    }

}
